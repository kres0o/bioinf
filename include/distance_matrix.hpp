#ifndef DISTANCE_MATRIX_HPP
#define DISTANCE_MATRIX_HPP

#include <vector>
#include <string>

class DistanceMatrix {
public:
    explicit DistanceMatrix(const std::vector<std::string>& sequences);
    double getDistance(int i, int j) const;
private:
    std::vector<std::vector<double>> matrix_;
    void calculateDistances(const std::vector<std::string>& sequences);
    double calculateDistance(const std::string& seq1, const std::string& seq2) const;
};

#endif // DISTANCE_MATRIX_HPP
