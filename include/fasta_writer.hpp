#ifndef FASTA_WRITER_HPP
#define FASTA_WRITER_HPP

#include <vector>
#include <string>

class FastaWriter {
public:
    explicit FastaWriter(const std::string& filename);
    void writeSequences(const std::vector<std::string>& sequences) const;
    void writeSequence(const std::string& header, const std::string& sequence) const; // Dodano
private:
    std::string filename_;
};

#endif // FASTA_WRITER_HPP
