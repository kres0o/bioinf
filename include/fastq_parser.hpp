#ifndef FASTQ_PARSER_HPP
#define FASTQ_PARSER_HPP

#include <vector>
#include <string>

class FastqParser {
public:
    explicit FastqParser(const std::string& filename);
    std::vector<std::string> getSequences() const;
private:
    std::vector<std::string> sequences_;
};

#endif // FASTQ_PARSER_HPP
