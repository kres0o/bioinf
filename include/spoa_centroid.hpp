#ifndef SPOA_CENTROID_HPP
#define SPOA_CENTROID_HPP

#include <string>
#include <vector>

std::string generateCentroid(const std::vector<std::string>& sequences);

#endif // SPOA_CENTROID_HPP
