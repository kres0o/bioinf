#ifndef CLUSTERING_HPP
#define CLUSTERING_HPP

#include <vector>
#include <string>
#include "distance_matrix.hpp"

class Clustering {
public:
    Clustering(int k, const std::vector<std::string>& sequences, const DistanceMatrix& distance_matrix);
    void performClustering();
    std::vector<std::vector<int>> getClusters() const;
    std::vector<std::string> getCentroids() const;
    void printClusters() const;
private:
    int k_;
    std::vector<std::string> sequences_;
    DistanceMatrix distance_matrix_;
    std::vector<std::vector<int>> clusters_;
    std::vector<std::string> centroids_;
};

#endif // CLUSTERING_HPP
