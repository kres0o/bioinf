CXX = g++
CXXFLAGS = -std=c++17 -Wall -Iinclude -I/usr/local/include -I/home/klinux/libs/spoa/include

LDFLAGS = -L/usr/local/lib -L/home/klinux/libs/spoa/build/src -lspoa

SRC = main.cpp \
      src/clustering.cpp \
      src/distance_matrix.cpp \
      src/fastq_parser.cpp \
      src/fasta_writer.cpp \
      src/spoa_centroid.cpp

INCLUDE = include

OBJ = $(SRC:.cpp=.o)
EXEC = bioinf

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CXX) $(CXXFLAGS) -I$(INCLUDE) -o $@ $^ $(LDFLAGS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -I$(INCLUDE) -c $< -o $@

clean:
	rm -f $(OBJ) $(EXEC)
