#include "fasta_writer.hpp"
#include <fstream>

FastaWriter::FastaWriter(const std::string& filename) : filename_(filename) {}

void FastaWriter::writeSequences(const std::vector<std::string>& sequences) const {
    std::ofstream file(filename_);
    for (size_t i = 0; i < sequences.size(); ++i) {
        file << ">sequence_" << i + 1 << "\n" << sequences[i] << "\n";
    }
}

void FastaWriter::writeSequence(const std::string& header, const std::string& sequence) const {
    std::ofstream file(filename_, std::ios_base::app); // Otvaranje datoteke u append modu
    file << ">" << header << "\n" << sequence << "\n";
}
