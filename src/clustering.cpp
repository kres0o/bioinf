#include "clustering.hpp"
#include "spoa_centroid.hpp"
#include <iostream> // Dodajte ovaj red

Clustering::Clustering(int k, const std::vector<std::string>& sequences, const DistanceMatrix& distance_matrix)
    : k_(k), sequences_(sequences), distance_matrix_(distance_matrix) {
    clusters_.resize(k_);
}

void Clustering::performClustering() {
    // Implementacija klasteriranja (npr. K-means)
    int n = sequences_.size();
    std::vector<int> labels(n, -1);
    std::vector<std::string> centroids(k_);
    
    // Inicijalizacija centroida (ovdje koristimo prve k sekvenci)
    for (int i = 0; i < k_; ++i) {
        centroids[i] = sequences_[i];
    }

    bool changed;
    do {
        changed = false;
        // Dodijeliti sekvencije klasterima
        for (int i = 0; i < n; ++i) {
            double min_distance = std::numeric_limits<double>::max();
            int min_index = -1;
            for (int j = 0; j < k_; ++j) {
                double distance = distance_matrix_.getDistance(i, j);
                if (distance < min_distance) {
                    min_distance = distance;
                    min_index = j;
                }
            }
            if (labels[i] != min_index) {
                labels[i] = min_index;
                changed = true;
            }
        }

        // Ažuriranje centroida
        for (int j = 0; j < k_; ++j) {
            std::vector<std::string> cluster_sequences;
            for (int i = 0; i < n; ++i) {
                if (labels[i] == j) {
                    cluster_sequences.push_back(sequences_[i]);
                }
            }
            centroids[j] = generateCentroid(cluster_sequences);
        }
    } while (changed);

    // Ažuriranje klastera
    for (int i = 0; i < k_; ++i) {
        clusters_[i].clear();
    }
    for (int i = 0; i < n; ++i) {
        clusters_[labels[i]].push_back(i);
    }
    centroids_ = centroids;
}

std::vector<std::vector<int>> Clustering::getClusters() const {
    return clusters_;
}

std::vector<std::string> Clustering::getCentroids() const {
    return centroids_;
}

void Clustering::printClusters() const {
    for (size_t i = 0; i < clusters_.size(); ++i) {
        std::cout << "Cluster " << i << ": "; // Ispravka
        for (size_t j = 0; j < clusters_[i].size(); ++j) {
            std::cout << clusters_[i][j] << " "; // Ispravka
        }
        std::cout << std::endl; // Ispravka
    }
}
