#include "fastq_parser.hpp"
#include <fstream>

FastqParser::FastqParser(const std::string& filename) {
    // Parsiranje FASTQ datoteke
    std::ifstream file(filename);
    std::string line;
    while (std::getline(file, line)) {
        if (line[0] == '@') {
            std::getline(file, line);
            sequences_.push_back(line);
            std::getline(file, line);
            std::getline(file, line);
        }
    }
}

std::vector<std::string> FastqParser::getSequences() const {
    return sequences_;
}
