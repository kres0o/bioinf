#include "spoa_centroid.hpp"
#include <spoa/spoa.hpp>

std::string generateCentroid(const std::vector<std::string>& sequences) {
    if (sequences.empty()) {
        return "";
    }

    // Kreiranje alignment enginea sa zadanim parametrima
    auto alignment_engine = spoa::AlignmentEngine::Create(
        spoa::AlignmentType::kNW,  // Koristi Needleman-Wunsch poravnanje
        2,  // Match score
        -4, // Mismatch score
        -8, // Gap opening score
        -6  // Gap extension score
    );

    // Kreiranje grafa
    auto graph = std::make_unique<spoa::Graph>();

    for (const auto& sequence : sequences) {
        // Poravnanje sekvence sa trenutnim grafom
        auto alignment = alignment_engine->Align(sequence, *graph);
        // Dodavanje poravnanja u graf
        graph->AddAlignment(alignment, sequence);
    }

    // Generiranje konsenzusa
    return graph->GenerateConsensus();
}
