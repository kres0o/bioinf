#include "distance_matrix.hpp"
#include <algorithm>
#include <vector>

DistanceMatrix::DistanceMatrix(const std::vector<std::string>& sequences) {
    calculateDistances(sequences);
}

void DistanceMatrix::calculateDistances(const std::vector<std::string>& sequences) {
    int n = sequences.size();
    matrix_.resize(n, std::vector<double>(n, 0.0));
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            double distance = calculateDistance(sequences[i], sequences[j]);
            matrix_[i][j] = distance;
            matrix_[j][i] = distance;
        }
    }
}

double DistanceMatrix::calculateDistance(const std::string& seq1, const std::string& seq2) const {
    const size_t len1 = seq1.size(), len2 = seq2.size();
    std::vector<std::vector<unsigned int>> d(len1 + 1, std::vector<unsigned int>(len2 + 1));

    d[0][0] = 0;
    for (unsigned int i = 1; i <= len1; ++i) d[i][0] = i;
    for (unsigned int i = 1; i <= len2; ++i) d[0][i] = i;

    for (unsigned int i = 1; i <= len1; ++i)
        for (unsigned int j = 1; j <= len2; ++j)
            d[i][j] = std::min({ d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + (seq1[i - 1] == seq2[j - 1] ? 0 : 1) });

    return d[len1][len2];
}

double DistanceMatrix::getDistance(int i, int j) const {
    return matrix_[i][j];
}
