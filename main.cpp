#include <iostream>
#include <vector>
#include <string>
#include "fastq_parser.hpp"
#include "fasta_writer.hpp"
#include "clustering.hpp"
#include "distance_matrix.hpp"

int main() {
    // Učitavanje sekvenci iz FASTQ datoteke
    std::cout << "Loading sequences from FASTQ file..." << std::endl;
    FastqParser parser("data/J29_B_CE_IonXpress_005.fastq");
    std::vector<std::string> sequences = parser.getSequences();

    // Ispis broja učitanih sekvenci
    std::cout << "Number of sequences read: " << sequences.size() << std::endl;

    // Postavljanje broja klastera
    int k = 3;
    std::cout << "Number of clusters: " << k << std::endl;

    // Kreiranje distance matrice
    std::cout << "Creating distance matrix..." << std::endl;
    DistanceMatrix distance_matrix(sequences);

    // Kreiranje clustering objekta
    std::cout << "Creating clustering object..." << std::endl;
    Clustering clustering(k, sequences, distance_matrix);

    // Izvođenje klasteriranja
    std::cout << "Performing clustering..." << std::endl;
    clustering.performClustering();

    // Dobivanje klastera
    std::cout << "Getting clusters..." << std::endl;
    std::vector<std::vector<int>> clusters = clustering.getClusters();
    clustering.printClusters();

    // Dobivanje centroida
    std::cout << "Getting centroids..." << std::endl;
    std::vector<std::string> centroids = clustering.getCentroids();
    std::cout << "Centroids: " << std::endl;
    for (const auto& centroid : centroids) {
        std::cout << centroid << std::endl;
    }

    // Pisanje rezultata u FASTA datoteku
    std::cout << "Writing results to FASTA file..." << std::endl;
    FastaWriter writer("output.fasta");
    writer.writeSequences(centroids);

    // Pisanje pojedinačnih sekvenci u FASTA datoteku
    for (size_t i = 0; i < clusters.size(); ++i) {
        for (int index : clusters[i]) {
            writer.writeSequence("Cluster" + std::to_string(i+1) + " - Sequence " + std::to_string(index+1), sequences[index]);
        }
    }

    return 0;
}
