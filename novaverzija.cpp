#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <cmath>
#include <spoa/spoa.hpp>

struct Sequence {
    std::string id;
    std::string seq;
};

std::vector<Sequence> read_fastq(const std::string &filename) {
    std::vector<Sequence> sequences;
    std::ifstream infile(filename);
    std::string line;
    Sequence seq;
    int line_num = 0;
    while (std::getline(infile, line)) {
        if (line_num % 4 == 0) {
            seq.id = line.substr(1); // Ukloni '@'
        } else if (line_num % 4 == 1) {
            seq.seq = line;
            sequences.push_back(seq);
        }
        line_num++;
    }
    return sequences;
}

std::vector<Sequence> filter_by_length(const std::vector<Sequence> &sequences, int length, int margin) {
    std::vector<Sequence> filtered;
    for (const auto &seq : sequences) {
        if (std::abs(static_cast<int>(seq.seq.size()) - length) <= margin) {
            filtered.push_back(seq);
        }
    }
    return filtered;
}

float calculate_hamming_distance(const std::string &seq1, const std::string &seq2) {
    if (seq1.size() != seq2.size()) {
        return 1.0f; // Najveća moguća udaljenost ako sekvence nisu iste duljine
    }

    int mismatches = 0;
    int length = seq1.size();
    for (int i = 0; i < length; ++i) {
        if (seq1[i] != seq2[i]) {
            mismatches++;
        }
    }
    return static_cast<float>(mismatches) / length;
}

std::unordered_map<int, std::vector<Sequence>> cluster_sequences(const std::vector<Sequence> &sequences, float distance_threshold) {
    std::unordered_map<int, std::vector<Sequence>> clusters;
    std::vector<int> cluster_ids(sequences.size(), -1);
    int cluster_id = 0;

    for (size_t i = 0; i < sequences.size(); ++i) {
        if (cluster_ids[i] == -1) {
            cluster_ids[i] = cluster_id++;
        }
        for (size_t j = i + 1; j < sequences.size(); ++j) {
            if (cluster_ids[j] == -1) {
                float distance = calculate_hamming_distance(sequences[i].seq, sequences[j].seq);
                if (distance <= distance_threshold) {
                    cluster_ids[j] = cluster_ids[i];
                }
            }
        }
    }

    for (size_t i = 0; i < sequences.size(); ++i) {
        clusters[cluster_ids[i]].push_back(sequences[i]);
    }

    return clusters;
}

std::string generate_consensus(const std::vector<Sequence> &sequences) {
    auto alignment_engine = spoa::AlignmentEngine::Create(
        spoa::AlignmentType::kNW,  // globalno poravnanje
        2,  // bod za podudaranje
        -45, // kazna za neusklađenost
        -100, // kazna za otvaranje praznine
        -57  // kazna za proširenje praznine
    );

    spoa::Graph graph;

    for (const auto &sequence : sequences) {
        auto alignment = alignment_engine->Align(sequence.seq, graph);
        graph.AddAlignment(alignment, sequence.seq);
    }

    return graph.GenerateConsensus();
}

void write_fasta(const std::string &filename, const std::vector<std::string> &consensus_sequences, const std::vector<int> &cluster_ids) {
    std::ofstream outfile(filename);
    for (size_t i = 0; i < consensus_sequences.size(); ++i) {
        outfile << ">consensus_" << i + 1 << " (from cluster " << cluster_ids[i] + 1 << ")\n";
        outfile << consensus_sequences[i] << "\n";
    }
}

void write_clusters(const std::string &filename, const std::unordered_map<int, std::vector<Sequence>> &clusters) {
    std::ofstream outfile(filename);
    for (const auto &cluster : clusters) {
        outfile << "Cluster " << cluster.first + 1 << " (size: " << cluster.second.size() << "):\n";
        for (const auto &seq : cluster.second) {
            outfile << seq.id << "\n";
        }
        outfile << "\n";
    }
}

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <input_fastq> [distance_threshold] [num_clusters]\n";
        return 1;
    }

    std::string input_fastq = argv[1];
    float distance_threshold = 0.36;  // Smanjeni prag udaljenosti
    int num_clusters = 2;  // Default number of largest clusters to output
    std::string output_fasta = "output.fasta";
    std::string output_clusters = "clusters.txt";

    if (argc >= 3) {
        distance_threshold = std::stof(argv[2]);
    }
    if (argc >= 4) {
        num_clusters = std::stoi(argv[3]);
    }

    // Korak 1: Učitaj sekvence iz FASTQ datoteke
    std::vector<Sequence> sequences = read_fastq(input_fastq);

    std::cout << "Ukupan broj sekvenci: " << sequences.size() << "\n";

    // Korak 2: Filtriraj sekvence po duljini (najčešća duljina +/- 5 baza)
    std::unordered_map<int, int> length_counts;
    for (const auto &seq : sequences) {
        length_counts[seq.seq.size()]++;
    }
    int most_common_length = std::max_element(
        length_counts.begin(), length_counts.end(),
        [](const std::pair<int, int>& a, const std::pair<int, int>& b) {
            return a.second < b.second;
        })->first;
    std::vector<Sequence> filtered_sequences = filter_by_length(sequences, most_common_length, 5);

    std::cout << "Ukupan broj filtriranih sekvenci: " << filtered_sequences.size() << "\n";

    // Korak 3: Klasteriziraj sekvence
    auto clusters = cluster_sequences(filtered_sequences, distance_threshold);

    std::cout << "Ukupan broj klastera: " << clusters.size() << "\n";

    // Ispis nekoliko sekvenci iz svakog klastera za provjeru
    for (const auto &cluster : clusters) {
        std::cout << "Klaster " << cluster.first + 1 << " (veličina: " << cluster.second.size() << "):\n";
        for (size_t i = 0; i < std::min(cluster.second.size(), size_t(5)); ++i) {
            std::cout << "  " << cluster.second[i].id << ": " << cluster.second[i].seq << "\n";
        }
    }

    // Korak 4: Sortiraj klastere po veličini i odaberi najveće
    std::vector<std::pair<int, std::vector<Sequence>>> cluster_vector(clusters.begin(), clusters.end());
    std::sort(cluster_vector.begin(), cluster_vector.end(),
              [](const auto &a, const auto &b) {
                  return a.second.size() > b.second.size();
              });

    // Provjera broja klastera i konsenzusa
    std::cout << "Broj klastera: " << cluster_vector.size() << "\n";

    // Korak 5: Generiraj konsenzusne sekvence za svaki klaster
    std::vector<std::string> consensus_sequences;
    std::vector<int> consensus_cluster_ids;
    for (const auto &cluster : cluster_vector) {
        std::cout << "Generiranje konsenzusa za klaster " << cluster.first + 1 << " (veličina: " << cluster.second.size() << ")\n";
        std::string consensus = generate_consensus(cluster.second);
        consensus_sequences.push_back(consensus);
        consensus_cluster_ids.push_back(cluster.first);
    }

    // Provjera broja generiranih konsenzusa
    std::cout << "Broj konsenzusnih sekvenci: " << consensus_sequences.size() << "\n";

    // Korak 6: Zapiši izlazne datoteke
    write_fasta(output_fasta, consensus_sequences, consensus_cluster_ids);
    write_clusters(output_clusters, clusters);

    // Korak 7: Ispis svih konsenzusnih sekvenci
    std::cout << "Konsenzusne sekvence:\n";
    for (size_t i = 0; i < consensus_sequences.size(); ++i) {
        std::cout << ">Konsenzus_" << i + 1 << " (iz klastera " << consensus_cluster_ids[i] + 1 << ")\n";
        std::cout << consensus_sequences[i] << "\n";
    }

    return 0;
}
